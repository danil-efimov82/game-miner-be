import { Request, Response } from 'express';

const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const dns = require("dns");
const ip = require("ip");


app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


app.get('/', function (req: any, res: any) {
  res.send('Hello World!');
});

async function isAvailable(name: string) {
  return new Promise((resolve, reject) => {
    dns.resolve4(name,function(err: any,addresses: string[]){
      if(err || !addresses){
        resolve(true);
      }

      resolve(false);
    });
  })
}

app.post('/api/check', async (req: Request, res: Response) => {
  const names = req.body;

  const anAsyncFunction = async (name: string) => {
    return {
      domain:  name,
      available: await isAvailable(name),
    }
  }

  const getData = async () => {
    return Promise.all(names.map((name: string) => anAsyncFunction(name)));
  }

  getData().then((data: any[]) => {
    res
      .send(data)
      .status(200)
      .end();
  });

});

app.listen(80, function () {
  console.log('Example app listening on port 3000! [IP]',  ip.address());
});
